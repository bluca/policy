# SOME DESCRIPTIVE TITLE.
# Copyright (C) 1996, 1997, 1998 Ian Jackson, Christian Schwarz, 1998-2017,
# The Debian Policy Mailing List
# This file is distributed under the same license as the Debian Policy
# Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Debian Policy Manual 4.1.6.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-17 20:06-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../../ch-archive.rst:2
msgid "The Debian Archive"
msgstr ""

#: ../../ch-archive.rst:4
msgid ""
"The Debian system is maintained and distributed as a collection of "
"*packages*. Since there are so many of them (currently well over 15000), "
"they are split into *sections* and given *priorities* to simplify the "
"handling of them."
msgstr ""

#: ../../ch-archive.rst:9
msgid ""
"The effort of the Debian project is to build a free operating system, but"
" not every package we want to make accessible is *free* in our sense (see"
" the Debian Free Software Guidelines, below), or may be imported/exported"
" without restrictions. Thus, the archive is split into areas  [#]_ based "
"on their licenses and other restrictions."
msgstr ""

#: ../../ch-archive.rst:15
msgid "The aims of this are:"
msgstr ""

#: ../../ch-archive.rst:17
msgid "to allow us to make as much software available as we can"
msgstr ""

#: ../../ch-archive.rst:19
msgid "to allow us to encourage everyone to write free software, and"
msgstr ""

#: ../../ch-archive.rst:21
msgid ""
"to allow us to make it easy for people to produce CD-ROMs of our system "
"without violating any licenses, import/export restrictions, or any other "
"laws."
msgstr ""

#: ../../ch-archive.rst:25
msgid "The *main* archive area forms the *Debian distribution*."
msgstr ""

#: ../../ch-archive.rst:27
msgid ""
"Packages in the other archive areas (``contrib``, ``non-free``) are not "
"considered to be part of the Debian distribution, although we support "
"their use and provide infrastructure for them (such as our bug-tracking "
"system and mailing lists). This Debian Policy Manual applies to these "
"packages as well."
msgstr ""

#: ../../ch-archive.rst:36
msgid "The Debian Free Software Guidelines"
msgstr ""

#: ../../ch-archive.rst:38
msgid ""
"The Debian Free Software Guidelines (DFSG) form our definition of \"free "
"software\". These are:"
msgstr ""

#: ../../ch-archive.rst:46
msgid "Free Redistribution"
msgstr ""

#: ../../ch-archive.rst:42
msgid ""
"The license of a Debian component may not restrict any party from selling"
" or giving away the software as a component of an aggregate software "
"distribution containing programs from several different sources. The "
"license may not require a royalty or other fee for such sale."
msgstr ""

#: ../../ch-archive.rst:50
msgid "Source Code"
msgstr ""

#: ../../ch-archive.rst:49
msgid ""
"The program must include source code, and must allow distribution in "
"source code as well as compiled form."
msgstr ""

#: ../../ch-archive.rst:55
msgid "Derived Works"
msgstr ""

#: ../../ch-archive.rst:53
msgid ""
"The license must allow modifications and derived works, and must allow "
"them to be distributed under the same terms as the license of the "
"original software."
msgstr ""

#: ../../ch-archive.rst:66
msgid "Integrity of The Author's Source Code"
msgstr ""

#: ../../ch-archive.rst:58
msgid ""
"The license may restrict source-code from being distributed in modified "
"form *only* if the license allows the distribution of \"patch files\" "
"with the source code for the purpose of modifying the program at build "
"time. The license must explicitly permit distribution of software built "
"from modified source code. The license may require derived works to carry"
" a different name or version number from the original software. (This is "
"a compromise. The Debian Project encourages all authors to not restrict "
"any files, source or binary, from being modified.)"
msgstr ""

#: ../../ch-archive.rst:70
msgid "No Discrimination Against Persons or Groups"
msgstr ""

#: ../../ch-archive.rst:69
msgid "The license must not discriminate against any person or group of persons."
msgstr ""

#: ../../ch-archive.rst:76
msgid "No Discrimination Against Fields of Endeavor"
msgstr ""

#: ../../ch-archive.rst:73
msgid ""
"The license must not restrict anyone from making use of the program in a "
"specific field of endeavor. For example, it may not restrict the program "
"from being used in a business, or from being used for genetic research."
msgstr ""

#: ../../ch-archive.rst:81
msgid "Distribution of License"
msgstr ""

#: ../../ch-archive.rst:79
msgid ""
"The rights attached to the program must apply to all to whom the program "
"is redistributed without the need for execution of an additional license "
"by those parties."
msgstr ""

#: ../../ch-archive.rst:89
msgid "License Must Not Be Specific to Debian"
msgstr ""

#: ../../ch-archive.rst:84
msgid ""
"The rights attached to the program must not depend on the program's being"
" part of a Debian system. If the program is extracted from Debian and "
"used or distributed without Debian but otherwise within the terms of the "
"program's license, all parties to whom the program is redistributed must "
"have the same rights as those that are granted in conjunction with the "
"Debian system."
msgstr ""

#: ../../ch-archive.rst:95
msgid "License Must Not Contaminate Other Software"
msgstr ""

#: ../../ch-archive.rst:92
msgid ""
"The license must not place restrictions on other software that is "
"distributed along with the licensed software. For example, the license "
"must not insist that all other programs distributed on the same medium "
"must be free software."
msgstr ""

#: ../../ch-archive.rst:99
msgid "Example Licenses"
msgstr ""

#: ../../ch-archive.rst:98
msgid ""
"The \"GPL,\" \"BSD,\" and \"Artistic\" licenses are examples of licenses "
"that we consider *free*."
msgstr ""

#: ../../ch-archive.rst:104
msgid "Archive areas"
msgstr ""

#: ../../ch-archive.rst:109
msgid "The main archive area"
msgstr ""

#: ../../ch-archive.rst:111
msgid ""
"The *main* archive area comprises the Debian distribution. Only the "
"packages in this area are considered part of the distribution. None of "
"the packages in the *main* archive area require software outside of that "
"area to function. Anyone may use, share, modify and redistribute the "
"packages in this archive area freely [#]_."
msgstr ""

#: ../../ch-archive.rst:117
msgid ""
"Every package in *main* must comply with the DFSG (Debian Free Software "
"Guidelines).  [#]_"
msgstr ""

#: ../../ch-archive.rst:120
msgid "In addition, the packages in *main*"
msgstr ""

#: ../../ch-archive.rst:122
msgid ""
"must not require or recommend a package outside of *main* for compilation"
" or execution (thus, the package must not declare a ``Pre-Depends``, "
"``Depends``, ``Recommends``, ``Build-Depends``, ``Build-Depends-Indep``, "
"or ``Build-Depends-Arch`` relationship on a non-*main* package unless "
"that package is only listed as a non-default alternative for a package in"
" *main*),"
msgstr ""

#: ../../ch-archive.rst:129 ../../ch-archive.rst:146 ../../ch-archive.rst:176
msgid "must not be so buggy that we refuse to support them, and"
msgstr ""

#: ../../ch-archive.rst:131 ../../ch-archive.rst:148
msgid "must meet all policy requirements presented in this manual."
msgstr ""

#: ../../ch-archive.rst:136
msgid "The contrib archive area"
msgstr ""

#: ../../ch-archive.rst:138
msgid ""
"The *contrib* archive area contains supplemental packages intended to "
"work with the Debian distribution, but which require software outside of "
"the distribution to either build or function."
msgstr ""

#: ../../ch-archive.rst:142
msgid "Every package in *contrib* must comply with the DFSG."
msgstr ""

#: ../../ch-archive.rst:144
msgid "In addition, the packages in *contrib*"
msgstr ""

#: ../../ch-archive.rst:150
msgid "Examples of packages which would be included in *contrib* are:"
msgstr ""

#: ../../ch-archive.rst:152
msgid ""
"free packages which require *contrib*, *non-free* packages or packages "
"which are not in our archive at all for compilation or execution, and"
msgstr ""

#: ../../ch-archive.rst:156
msgid "wrapper packages or other sorts of free accessories for non-free programs."
msgstr ""

#: ../../ch-archive.rst:162
msgid "The non-free archive area"
msgstr ""

#: ../../ch-archive.rst:164
msgid ""
"The *non-free* archive area contains supplemental packages intended to "
"work with the Debian distribution that do not comply with the DFSG or "
"have other problems that make their distribution problematic. They may "
"not comply with all of the policy requirements in this manual due to "
"restrictions on modifications or other limitations."
msgstr ""

#: ../../ch-archive.rst:170
msgid ""
"Packages must be placed in *non-free* if they are not compliant with the "
"DFSG or are encumbered by patents or other legal issues that make their "
"distribution problematic."
msgstr ""

#: ../../ch-archive.rst:174
msgid "In addition, the packages in *non-free*"
msgstr ""

#: ../../ch-archive.rst:178
msgid ""
"must meet all policy requirements presented in this manual that it is "
"possible for them to meet.  [#]_"
msgstr ""

#: ../../ch-archive.rst:184
msgid "Copyright considerations"
msgstr ""

#: ../../ch-archive.rst:186
msgid ""
"Every package must be accompanied by a verbatim copy of its distribution "
"license(s) in the file ``/usr/share/doc/PACKAGE/copyright``."
msgstr ""

#: ../../ch-archive.rst:189
msgid ""
"The copyright information for files in a package must be copied verbatim "
"into ``/usr/share/doc/PACKAGE/copyright``, when all of the following "
"hold:"
msgstr ""

#: ../../ch-archive.rst:193
msgid ""
"the distribution license for those files requires that copyright "
"information be included in all copies and/or binary distributions;"
msgstr ""

#: ../../ch-archive.rst:196
msgid ""
"the files are shipped in the binary package, either in source or compiled"
" form; and"
msgstr ""

#: ../../ch-archive.rst:199
msgid ""
"the form in which the files are present in the binary package does not "
"include a plain text version of their copyright notices."
msgstr ""

#: ../../ch-archive.rst:202
msgid ""
"Thus, the copyright information for files in the source package which are"
" only part of its build process, such as autotools files, need not be "
"included in ``/usr/share/doc/PACKAGE/copyright``, because those files do "
"not get installed into the binary package.  Similarly, plain text files "
"which include their own copyright information and are installed into the "
"binary package unmodified need not have that copyright information copied"
" into ``/usr/share/doc/PACKAGE/copyright``"
msgstr ""

#: ../../ch-archive.rst:210
msgid ""
"However, the copyright notices for any files which are compiled into the "
"object code shipped in the binary package must all be included in "
"``/usr/share/doc/PACKAGE/copyright`` when the license requires that "
"copyright information be included in all copies and/or binary "
"distributions, as most do.  [#]_"
msgstr ""

#: ../../ch-archive.rst:216
msgid "See :ref:`s-copyrightfile` for further details."
msgstr ""

#: ../../ch-archive.rst:218
msgid ""
"We reserve the right to restrict files from being included anywhere in "
"our archives if"
msgstr ""

#: ../../ch-archive.rst:221
msgid "their use or distribution would break a law,"
msgstr ""

#: ../../ch-archive.rst:223
msgid "there is an ethical conflict in their distribution or use,"
msgstr ""

#: ../../ch-archive.rst:225
msgid "we would have to sign a license for them, or"
msgstr ""

#: ../../ch-archive.rst:227
msgid "their distribution would conflict with other project policies."
msgstr ""

#: ../../ch-archive.rst:229
msgid ""
"Programs whose authors encourage the user to make donations are fine for "
"the main distribution, provided that the authors do not claim that not "
"donating is immoral, unethical, illegal or something similar; in such a "
"case they must go in *non-free*."
msgstr ""

#: ../../ch-archive.rst:234
msgid ""
"Packages whose copyright permission notices (or patent problems) do not "
"even allow redistribution of binaries only, and where no special "
"permission has been obtained, must not be placed on the Debian FTP site "
"and its mirrors at all."
msgstr ""

#: ../../ch-archive.rst:239
msgid ""
"Note that under international copyright law (this applies in the United "
"States, too), *no* distribution or modification of a work is allowed "
"without an explicit notice saying so. Therefore a program without a "
"copyright notice *is* copyrighted and you may not do anything to it "
"without risking being sued! Likewise if a program has a copyright notice "
"but no statement saying what is permitted then nothing is permitted."
msgstr ""

#: ../../ch-archive.rst:246
msgid ""
"Many authors are unaware of the problems that restrictive copyrights (or "
"lack of copyright notices) can cause for the users of their supposedly-"
"free software. It is often worthwhile contacting such authors "
"diplomatically to ask them to modify their license terms. However, this "
"can be a politically difficult thing to do and you should ask for advice "
"on the ``debian-legal`` mailing list first, as explained below."
msgstr ""

#: ../../ch-archive.rst:253
msgid ""
"When in doubt about a copyright, send mail to debian-"
"legal@lists.debian.org. Be prepared to provide us with the copyright "
"statement. Software covered by the GPL, public domain software and BSD-"
"like copyrights are safe; be wary of the phrases \"commercial use "
"prohibited\" and \"distribution restricted\"."
msgstr ""

#: ../../ch-archive.rst:262
msgid "Sections"
msgstr ""

#: ../../ch-archive.rst:264
msgid ""
"The packages in the archive areas *main*, *contrib* and *non-free* are "
"grouped further into *sections* to simplify handling."
msgstr ""

#: ../../ch-archive.rst:267
msgid ""
"The archive area and section for each package should be specified in the "
"package's ``Section`` control record (see :ref:`s-f-Section`). However, "
"the maintainer of the Debian archive may override this selection to "
"ensure the consistency of the Debian distribution. The ``Section`` field "
"should be of the form:"
msgstr ""

#: ../../ch-archive.rst:273
msgid "*section* if the package is in the *main* archive area,"
msgstr ""

#: ../../ch-archive.rst:275
msgid ""
"*area/section* if the package is in the *contrib* or *non-free* archive "
"areas."
msgstr ""

#: ../../ch-archive.rst:278
msgid ""
"The Debian archive maintainers provide the authoritative list of "
"sections. At present, they are: admin, cli-mono, comm, database, debug, "
"devel, doc, editors, education, electronics, embedded, fonts, games, "
"gnome, gnu-r, gnustep, graphics, hamradio, haskell, httpd, interpreters, "
"introspection, java, javascript, kde, kernel, libdevel, libs, lisp, "
"localization, mail, math, metapackages, misc, net, news, ocaml, oldlibs, "
"otherosfs, perl, php, python, ruby, rust, science, shells, sound, tasks, "
"tex, text, utils, vcs, video, web, x11, xfce, zope. The additional "
"section *debian-installer* contains special packages used by the "
"installer and is not used for normal Debian packages."
msgstr ""

#: ../../ch-archive.rst:289
msgid ""
"For more information about the sections and their definitions, see the "
"`list of sections in unstable <https://packages.debian.org/unstable/>`_."
msgstr ""

#: ../../ch-archive.rst:296
msgid "Priorities"
msgstr ""

#: ../../ch-archive.rst:298
msgid ""
"Each package must have a *priority* value, which is set in the metadata "
"for the Debian archive and is also included in the package's control "
"files (see :ref:`s-f-Priority`). This information is used to control "
"which packages are included in standard or minimal Debian installations."
msgstr ""

#: ../../ch-archive.rst:304
msgid ""
"Most Debian packages will have a priority of ``optional``. Priority "
"levels other than ``optional`` are only used for packages that should be "
"included by default in a standard installation of Debian."
msgstr ""

#: ../../ch-archive.rst:308
msgid ""
"The priority of a package is determined solely by the functionality it "
"provides directly to the user. The priority of a package should not be "
"increased merely because another higher-priority package depends on it; "
"instead, the tools used to construct Debian installations will correctly "
"handle package dependencies. In particular, this means that C-like "
"libraries will almost never have a priority above ``optional``, since "
"they do not provide functionality directly to users. However, as an "
"exception, the maintainers of Debian installers may request an increase "
"of the priority of a package to resolve installation issues and ensure "
"that the correct set of packages is included in a standard or minimal "
"install."
msgstr ""

#: ../../ch-archive.rst:320
msgid ""
"The following *priority levels* are recognized by the Debian package "
"management tools."
msgstr ""

#: ../../ch-archive.rst:332
msgid "``required``"
msgstr ""

#: ../../ch-archive.rst:324
msgid ""
"Packages which are necessary for the proper functioning of the system "
"(usually, this means that dpkg functionality depends on these packages). "
"Removing a ``required`` package may cause your system to become totally "
"broken and you may not even be able to use ``dpkg`` to put things back, "
"so only do so if you know what you are doing."
msgstr ""

#: ../../ch-archive.rst:330
msgid ""
"Systems with only the ``required`` packages installed have at least "
"enough functionality for the sysadmin to boot the system and install more"
" software."
msgstr ""

#: ../../ch-archive.rst:343
msgid "``important``"
msgstr ""

#: ../../ch-archive.rst:335
msgid ""
"Important programs, including those which one would expect to find on any"
" Unix-like system. If the expectation is that an experienced Unix person "
"who found it missing would say \"What on earth is going on, where is "
"``foo``?\", it must be an ``important`` package.  [#]_ Other packages "
"without which the system will not run well or be usable must also have "
"priority ``important``. This does *not* include Emacs, the X Window "
"System, TeX or any other large applications. The ``important`` packages "
"are just a bare minimum of commonly-expected and necessary tools."
msgstr ""

#: ../../ch-archive.rst:352
msgid "``standard``"
msgstr ""

#: ../../ch-archive.rst:346
msgid ""
"These packages provide a reasonably small but not too limited character-"
"mode system. This is what will be installed by default if the user "
"doesn't select anything else. It doesn't include many large applications."
msgstr ""

#: ../../ch-archive.rst:351
msgid ""
"Two packages that both have a priority of ``standard`` or higher must not"
" conflict with each other."
msgstr ""

#: ../../ch-archive.rst:358
msgid "``optional``"
msgstr ""

#: ../../ch-archive.rst:355
msgid ""
"This is the default priority for the majority of the archive. Unless a "
"package should be installed by default on standard Debian systems, it "
"should have a priority of ``optional``. Packages with a priority of "
"``optional`` may conflict with each other."
msgstr ""

#: ../../ch-archive.rst:369
msgid "``extra``"
msgstr ""

#: ../../ch-archive.rst:361
msgid ""
"*This priority is deprecated.* Use the ``optional`` priority instead. "
"This priority should be treated as equivalent to ``optional``."
msgstr ""

#: ../../ch-archive.rst:365
msgid ""
"The ``extra`` priority was previously used for packages that conflicted "
"with other packages and packages that were only likely to be useful to "
"people with specialized requirements. However, this distinction was "
"somewhat arbitrary, not consistently followed, and not useful enough to "
"warrant the maintenance effort."
msgstr ""

#: ../../ch-archive.rst:372
msgid ""
"The Debian archive software uses the term \"component\" internally and in"
" the Release file format to refer to the division of an archive. The "
"Debian Social Contract simply refers to \"areas.\" This document uses "
"terminology similar to the Social Contract."
msgstr ""

#: ../../ch-archive.rst:378
msgid ""
"See `What Does Free Mean? <https://www.debian.org/intro/free>`_ for more "
"about what we mean by free software."
msgstr ""

#: ../../ch-archive.rst:382
msgid ""
"Debian's FTP Masters publish a `REJECT-FAQ <https://ftp-master.debian.org"
"/REJECT-FAQ.html>`_ which details the project's current working "
"interpretation of the DFSG."
msgstr ""

#: ../../ch-archive.rst:387
msgid ""
"It is possible that there are policy requirements which the package is "
"unable to meet, for example, if the source is unavailable. These "
"situations will need to be handled on a case-by-case basis."
msgstr ""

#: ../../ch-archive.rst:392
msgid ""
"Licenses that are not thought to require the copying of all copyright "
"notices into Debian's copyright file include Apache-2.0 and the Boost "
"Software License, version 1.0.  Final determination as to whether a "
"package's copyright file is sufficient lies with the FTP team."
msgstr ""

#: ../../ch-archive.rst:398
msgid ""
"To help find copyright notices you need to copy, you might try ``grep "
"--color=always -Eir '(copyright|©)' * | less -R``"
msgstr ""

#: ../../ch-archive.rst:402
msgid ""
"This is an important criterion because we are trying to produce, amongst "
"other things, a free Unix."
msgstr ""

#~ msgid ""
#~ "Example Licenses The \"GPL,\" \"BSD,\" "
#~ "and \"Artistic\" licenses are examples "
#~ "of licenses that we consider *free*."
#~ msgstr ""

#~ msgid ""
#~ "Every package must be accompanied by "
#~ "a verbatim copy of its copyright "
#~ "information and distribution license in "
#~ "the file ``/usr/share/doc/package/copyright`` (see"
#~ " :ref:`s-copyrightfile` for further details)."
#~ msgstr ""

#~ msgid ""
#~ "Every package must be accompanied by "
#~ "a verbatim copy of its copyright "
#~ "information, unless its distribution license"
#~ " explicitly permits this information to "
#~ "be excluded from distributions of "
#~ "binaries built from the source.  In "
#~ "such cases, a verbatim copy of its"
#~ " copyright information should normally "
#~ "still be included, but need not be"
#~ " if creating and maintaining a copy"
#~ " of that information involves significant"
#~ " time and effort."
#~ msgstr ""

#~ msgid ""
#~ "Every package must be accompanied by "
#~ "a verbatim copy of its distribution "
#~ "license in the file "
#~ "``/usr/share/doc/package/copyright``."
#~ msgstr ""

#~ msgid ""
#~ "Every package must be accompanied by "
#~ "a verbatim copy of its copyright "
#~ "information, unless its distribution license"
#~ " explicitly permits this information to "
#~ "be excluded from distributions of "
#~ "binaries built from the source.  In "
#~ "such cases, a verbatim copy of its"
#~ " copyright information should normally "
#~ "still be included, but need not be"
#~ " if creating and maintaining a copy"
#~ " of that information involves significant"
#~ " time and effort.  [#]_"
#~ msgstr ""

#~ msgid ""
#~ "No two packages that both have a"
#~ " priority of ``standard`` or higher "
#~ "may conflict with each other."
#~ msgstr ""

