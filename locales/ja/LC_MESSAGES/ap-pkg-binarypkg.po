# SOME DESCRIPTIVE TITLE.
# Copyright (C) 1996, 1997, 1998 Ian Jackson, Christian Schwarz, 1998-2017,
# The Debian Policy Mailing List
# This file is distributed under the same license as the Debian Policy
# Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Debian Policy Manual 4.1.6.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-11-29 18:08-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.6.0\n"

#: ../../ap-pkg-binarypkg.rst:2
msgid "Binary packages (from old Packaging Manual)"
msgstr ""

#: ../../ap-pkg-binarypkg.rst:4
msgid "See :manpage:`deb(5)` and :ref:`s-pkg-controlarea`."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:9
msgid "Creating package files - ``dpkg-deb``"
msgstr ""

#: ../../ap-pkg-binarypkg.rst:11
msgid ""
"All manipulation of binary package files is done by ``dpkg-deb``; it's "
"the only program that has knowledge of the format. (``dpkg-deb`` may be "
"invoked by calling ``dpkg``, as ``dpkg`` will spot that the options "
"requested are appropriate to ``dpkg-deb`` and invoke that instead with "
"the same arguments.)"
msgstr ""

#: ../../ap-pkg-binarypkg.rst:17
msgid ""
"In order to create a binary package, you must make a directory tree which"
" contains all the files and directories you want to have in the file "
"system data part of the package. In Debian-format source packages, this "
"directory is usually either ``debian/tmp`` or ``debian/pkg``, relative to"
" the top of the package's source tree."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:23
msgid ""
"They should have the locations (relative to the root of the directory "
"tree you're constructing) ownerships and permissions which you want them "
"to have on the system when they are installed."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:27
msgid ""
"With current versions of ``dpkg`` the uid/username and gid/groupname "
"mappings for the users and groups being used should be the same on the "
"system where the package is built and the one where it is installed."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:31
msgid ""
"You need to add one special directory to the root of the miniature file "
"system tree you're creating: ``DEBIAN``. It should contain the control "
"information files, notably the binary package control file (see :ref:`s"
"-pkg-controlfile`)."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:36
msgid ""
"The ``DEBIAN`` directory will not appear in the file system archive of "
"the package, and so won't be installed by ``dpkg`` when the package is "
"unpacked."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:40
msgid "When you've prepared the package, you should invoke:"
msgstr ""

#: ../../ap-pkg-binarypkg.rst:46
msgid ""
"This will build the package in ``directory.deb``. (``dpkg`` knows that "
"``--build`` is a ``dpkg-deb`` option, so it invokes ``dpkg-deb`` with the"
" same arguments to build the package.)"
msgstr ""

#: ../../ap-pkg-binarypkg.rst:50
msgid ""
"See the :manpage:`dpkg-deb(8)` man page for details of how to examine the"
" contents of this newly-created file. You may find the output of "
"following commands enlightening:"
msgstr ""

#: ../../ap-pkg-binarypkg.rst:60
msgid "To view the copyright file for a package you could use this command:"
msgstr ""

#: ../../ap-pkg-binarypkg.rst:69
msgid "Package control information files"
msgstr ""

#: ../../ap-pkg-binarypkg.rst:71
msgid ""
"The control information portion of a binary package is a collection of "
"files with names known to ``dpkg``. It will treat the contents of these "
"files specially - some of them contain information used by ``dpkg`` when "
"installing or removing the package; others are scripts which the package "
"maintainer wants ``dpkg`` to run."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:77
msgid ""
"It is possible to put other files in the package control information file"
" area, but this is not generally a good idea (though they will largely be"
" ignored)."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:81
msgid ""
"Here is a brief list of the control information files supported by "
"``dpkg`` and a summary of what they're used for."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:93
msgid "``control``"
msgstr ""

#: ../../ap-pkg-binarypkg.rst:85
msgid ""
"This is the key description file used by ``dpkg``. It specifies the "
"package's name and version, gives its description for the user, states "
"its relationships with other packages, and so forth. See "
":ref:`s-sourcecontrolfiles` and :ref:`s-binarycontrolfiles`."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:91
msgid ""
"It is usually generated automatically from information in the source "
"package by the ``dpkg-gencontrol`` program, and with assistance from "
"``dpkg-shlibdeps``. See :doc:`ap-pkg-sourcepkg`."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:108
msgid "``postinst``, ``preinst``, ``postrm``, ``prerm``"
msgstr ""

#: ../../ap-pkg-binarypkg.rst:96
msgid ""
"These are executable files (usually scripts) which ``dpkg`` runs during "
"installation, upgrade and removal of packages. They allow the package to "
"deal with matters which are particular to that package or require more "
"complicated processing than that provided by ``dpkg``. Details of when "
"and how they are called are in :doc:`Package maintainer scripts and "
"installation procedure <ch-maintainerscripts>`."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:103
msgid ""
"It is very important to make these scripts idempotent. See "
":ref:`s-idempotency`."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:106
msgid ""
"The maintainer scripts are not guaranteed to run with a controlling "
"terminal and may not be able to interact with the user. See "
":ref:`s-controllingterminal`."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:113
msgid "``conffiles`` This file contains a list of configuration files which"
msgstr ""

#: ../../ap-pkg-binarypkg.rst:111
msgid ""
"are to be handled automatically by ``dpkg`` (see :doc:`ap-pkg-"
"conffiles`). Note that not necessarily every configuration file should be"
" listed here."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:120
msgid "``shlibs``"
msgstr ""

#: ../../ap-pkg-binarypkg.rst:116
msgid ""
"This file contains a list of the shared libraries supplied by the "
"package, with dependency details for each. This is used by ``dpkg-"
"shlibdeps`` when it determines what dependencies are required in a "
"package control file. The ``shlibs`` file format is described on "
":ref:`s-shlibs`."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:125
msgid "The main control information file: ``control``"
msgstr ""

#: ../../ap-pkg-binarypkg.rst:127
msgid ""
"The most important control information file used by ``dpkg`` when it "
"installs a package is ``control``. It contains all the package's \"vital "
"statistics\"."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:131
msgid ""
"The binary package control files of packages built from Debian sources "
"are made by a special tool, ``dpkg-gencontrol``, which reads "
"``debian/control`` and ``debian/changelog`` to find the information it "
"needs. See :doc:`ap-pkg-sourcepkg` for more details."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:136
msgid ""
"The fields in binary package control files are listed in "
":ref:`s-binarycontrolfiles`."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:139
msgid ""
"A description of the syntax of control files and the purpose of the "
"fields is available in :doc:`Control files and their fields <ch-"
"controlfields>`."
msgstr ""

#: ../../ap-pkg-binarypkg.rst:146
msgid "Time Stamps"
msgstr ""

#: ../../ap-pkg-binarypkg.rst:148
msgid "See :ref:`s-timestamps`."
msgstr ""

#~ msgid "See deb(5) and :ref:`s-pkg-controlarea`."
#~ msgstr ""

#~ msgid ""
#~ "See the dpkg-deb(8) man page for"
#~ " details of how to examine the "
#~ "contents of this newly-created file. "
#~ "You may find the output of "
#~ "following commands enlightening:"
#~ msgstr ""

